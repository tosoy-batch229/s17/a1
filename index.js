/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
    // First function here:
    let getPersonalInfo = () => {
        let getFullName = prompt("Enter Fullname: ");
        let getAge = prompt("Enter Age: ");
        let getLocation = prompt("Enter Location: ");

        console.log(`Hello, ${getFullName}`);
        console.log(`You are ${getAge} years old.`);
        console.log(`You live in ${getLocation}`);
    }

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
 
    // Second function here:
    let getFavoriteBands = () => {
        const bandNames = [];

        for (let i = 0; i < 5; i++) {
            bandNames[i] = prompt(`Enter Your Favorite Band #${i + 1}`);
        }

        for (let i = 0; i < 5; i++) {
            console.log(`${i + 1}. ${bandNames[i]}`);
        }
    }

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
    // Third function here:
    let getFavoriteMovies = () => {
        const movies = [];
        const ratings = [];

        for (let i = 0; i < 5; i++) {
            movies[i] = prompt(`Enter Your Favorite Movie #${i + 1}`);
            ratings[i] = prompt(`Enter its Rotten Tomatoes Rating: `);
        }

        for (let i = 0; i < 5; i++) {
            console.log(`${i + 1}. ${movies[i]}`);
            console.log(`Rotten Tomatoes Rating: ${ratings[i]}%`);
        }
    }

    

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

    // Fourth function here:
    let printFriends = () => {
        alert("Hi! Please add the names of your friends.");
        let friend1 = prompt("Enter your first friend's name:"); 
        let friend2 = prompt("Enter your second friend's name:"); 
        let friend3 = prompt("Enter your third friend's name:");

        console.log("You are friends with:")
        console.log(friend1); 
        console.log(friend2); 
        console.log(friend3); 
    };

    // Functions Invocations
    getPersonalInfo();
    getFavoriteBands();
    getFavoriteMovies();
    printFriends();